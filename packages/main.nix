{ pkgs, ... }:

with pkgs;
let
in {
  home.packages = with pkgs; [
    # MISC
    arandr
    powertop
    arc-theme
    bibata-cursors
    volumeicon
    playerctl
    brightnessctl

    # TERMINAL
    neofetch
    zip
    unrar
    unzip
    wget
    git
    exa         #Replace ls
    bat         #Replace cat
    fd          #Replace find
    any-nix-shell
    htop

    # Browser
    librewolf
    brave
    thunderbird

    # EDITOR
    geany
    
    # DEFAULT
    pavucontrol
    signal-desktop
    vlc
    spotify
    blueman
    gimp
    sxiv
    cinnamon.xreader
    #teams

    # TOOLS
    gparted
    cinnamon.nemo
    keepassxc
    citrix_workspace
    libreoffice-fresh
    qalculate-gtk
    veracrypt
    virtualbox
    #kdenlive
    #appimage-run
    #rustdesk

    # DEVELOP
    #jetbrains.idea-ultimate
    #jdk
    #nodejs
    #dbeaver
    #android-tools

    # GAMES
    minecraft
    steam

    # OPENBOX
    openbox
    obconf
    lxappearance
    tint2
    compton
    nitrogen
    rofi
    menumaker 
    i3lock
  ];

  services = {                            # Applets
    blueman-applet.enable = true;         # Bluetooth
  };
}
