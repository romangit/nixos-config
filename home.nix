{ config, lib, pkgs, ... }:

let
  username = "roman-nix";
  homeDirectory = "/home/${username}";
in
{
  programs.home-manager.enable = true;

  imports = [./configs/main.nix ./packages/main.nix]; 

  home = {
    inherit username homeDirectory;
    stateVersion = "22.11";

    sessionVariables = {
      TERMINAL = "alacritty";
      EDITOR = "nvim";
      VISUAL = "nvim";
    };
  };
  
  # activate unfree packages
  nixpkgs.config.allowUnfree = true;

  # restart services on change
  systemd.user.startServices = "sd-switch";

  # notifications about home-manager news
  news.display = "silent";

  fonts.fontconfig.enable = true;

  # add fonts
  home.packages = with pkgs; [
    carlito                                 # NixOS
    vegur                                   # NixOS
    source-code-pro
    jetbrains-mono
    font-awesome                            # Icons
    corefonts                               # MS
  ];
}
