{ config, pkgs, lib, ... }:

{
  imports = [
    ./programs/alacritty/alacritty.nix
    ./services/redshift/redshift.nix
    ./shell/zsh/zsh.nix
    ./editors/nvim/nvim.nix
    ./programs/rofi/rofi.nix
  ];

  programs = {
    home-manager.enable = true;
    command-not-found.enable = true;
  };
  
  home.file.".config/openbox" = {
    source = ./dotfiles/openbox;
    recursive = true;
  };
  home.file.".config/tint2/tint2rc" = {
    source = ./dotfiles/tint2/tint2rc;
  };
  home.file.".themes/Box-Dark" = {
    source = ./dotfiles/Box-Dark;
    recursive = true;
  };
}
