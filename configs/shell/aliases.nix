{
### ALIASES ###
# \x1b[2J   <- clears tty
# \x1b[1;1H <- goes to (1, 1) (start)

# root privileges
doas="doas --";

# vim and emacs
vim="nvim";

# Changing "ls" to "exa"
ls="exa -al --color=always --group-directories-first"; # my preferred listing
la="exa -a --color=always --group-directories-first";  # all files and dirs
ll="exa -l --color=always --group-directories-first";  # long format
lt="exa -aT --color=always --group-directories-first"; # tree listing

# Colorize grep output (good for log files)
grep="grep --color=auto";
egrep="egrep --color=auto";
fgrep="fgrep --color=auto";

# confirm before overwriting something
cp="cp -i";
mv="mv -i";
rm="rm -i";

# ps
psa="ps auxf";
psgrep="ps aux | grep -v grep | grep -i -e VSZ -e";
psmem="ps auxf | sort -nr -k 4";
pscpu="ps auxf | sort -nr -k 3";

# Merge Xresources
merge="xrdb -merge ~/.Xresources";

# git
addup="git add -u";
addall="git add .";
branch="git branch";
checkout="git checkout";
clone="git clone";
commit="git commit -m";
fetch="git fetch";
pull="git pull origin";
push="git push origin";
tag="git tag";
newtag="git tag -a";

# get error messages from journalctl
jctl="journalctl -p 3 -xb";

# vpn
vpnup="wg-quick up roman-laptop";
vpndown="wg-quick down roman-laptop";
}
