## How to use for unstable NixOS

# Install Config Home-Manager

```
cd ~
git clone https://gitlab.com/romangit/nixos-config ~/.config/nixpkgs
mv ~/.config/nixpkgs/linuxx64-22.12.0.12.tar.gz ~
nix-prefetch-url file://$PWD/linuxx64-22.12.0.12.tar.gz

```

Edit username and homeDirectory in ~.config/nixpkgs/home.nix

# Install Home-Manager

```
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

nix-shell '<home-manager>' -A install

home-manager switch

```

# Edit configuration.nix

```
sudo nvim /etc/nixos/configuration.nix
#add extraGroups "video" to user
# add Code at end
sudo nixos-rebuild switch

```

# Generate menu for openbox

```
mmaker openbox -t alacritty -f

```

# Code for configuration.nix
```
# self generated
  networking.firewall.enable = true;
  
  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  services.xserver.windowManager.openbox.enable = true;
  programs.steam.enable = true;

```

# Important commands

```
# Delete old savepoints
sudo nix-collect-garbage -d 
# temporary package install
nix-shell -p "package"
# Nix-Update
sudo nix-channel --update
sudo nixos-rebuild --upgrade boot
# Update for Home-Manager
nix-channel --update
# Add wireguard VPN
nmcli connection import type wireguard file thefile.conf
```